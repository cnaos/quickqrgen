package qucickqrgen

import groovy.beans.Bindable

class QucickQrGenModel {
   @Bindable boolean enabled = true
   @Bindable String qrText
   @Bindable String level
}