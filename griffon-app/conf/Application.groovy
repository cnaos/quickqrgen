application {
    title = 'QucickQrGen'
    startupGroups = ['qucickQrGen']

    // Should Griffon exit when no Griffon created frames are showing?
    autoShutdown = true

    // If you want some non-standard application class, apply it here
    //frameClass = 'javax.swing.JFrame'
}
mvcGroups {
    // MVC Group for "qucickQrGen"
    'qucickQrGen' {
        model      = 'qucickqrgen.QucickQrGenModel'
        view       = 'qucickqrgen.QucickQrGenView'
        controller = 'qucickqrgen.QucickQrGenController'
    }

}
