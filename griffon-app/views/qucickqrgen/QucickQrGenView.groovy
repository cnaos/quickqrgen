package qucickqrgen

import java.awt.BorderLayout as BL
import javax.swing.WindowConstants as WC
import javax.swing.BorderFactory as BF

actions {
action(id:'paint',
	name: 'Paint',
	enabled: bind{ model.enabled },
	closure: controller.paintQR,
	mnemonic: 'P',
	accelerator: 'ctrl P')

action(id:'about',
	name: 'About',
	closure: controller.showAbout,
	mnemonic: 'A',
	accelerator: 'F1')

action(id:'levelL',
	name: 'L(7%)',
	actionCommandKey: 'L',
	closure: controller.paintQR
	)

action(id:'levelM',
	name: 'M(15%)',
	actionCommandKey: 'M',
	closure: controller.paintQR
	)

action(id:'levelQ',
	name: 'Q(25%)',
	actionCommandKey: 'Q',
	closure: controller.paintQR
	)

action(id:'levelH',
	name: 'H(30%)',
	actionCommandKey: 'H',
	closure: controller.paintQR
	)

}

buttonGroup( id: 'btns')

application(
  id: 'appFrame',
  title: 'QucickQrGen',
  pack: true,
  locationByPlatform:true,
  iconImage: imageIcon('/griffon-icon-48x48.png').image,
  iconImages: [imageIcon('/griffon-icon-48x48.png').image,
               imageIcon('/griffon-icon-32x32.png').image,
               imageIcon('/griffon-icon-16x16.png').image]) {
    // add content here
    menuBar(){
		menu(mnemonic:'A', 'Action'){
			menuItem(action:paint)
		}
		glue()
		menu(mnemonic:'H', 'Help'){
			menuItem(action:about)
		}
	}

	panel(border:BF.createEmptyBorder(6, 6, 6, 6)){
	   borderLayout()
	   vbox(constraints: BL.NORTH){
		   hbox{
			   hstrut(width:10)
			   textField(id:'tf',
				   action: paint,
				   columns:20,
				   text: bind(target: model, 'qrText'),
				   'input text here')

		   }
		   panel(border:BF.createTitledBorder('Level')){
			   radioButton( id: 'L', text: 'L(7%)',  action:levelL, buttonGroup: btns, focusable:false, selected: true )
			   radioButton( id: 'M', text: 'M(15%)', action:levelM, buttonGroup: btns, focusable:false )
			   radioButton( id: 'Q', text: 'Q(25%)', action:levelQ, buttonGroup: btns, focusable:false )
			   radioButton( id: 'H', text: 'H(30%)', action:levelH, buttonGroup: btns, focusable:false )
		   }
	   }
	   vbox(constraints: BL.CENTER, border:BF.createTitledBorder('QR Code')){
		   text:bind{model.qrText}
		   panel(id:'canvas')
	   }
//	   vbox(constraints: BL.SOUTH){
//		  label(text: bind{  model.qrText }, 'sample' )
//		  label(text: bind{  model.level }, 'sample' )
//	   }
	}
}
bean( model, qrText:bind {tf.text})
