package qucickqrgen

import javax.swing.ImageIcon
import javax.swing.JLabel


class QucickQrGenController {
    // these will be injected by Griffon
    def model
    def view
	def qrCodeGenerateService

    // void mvcGroupInit(Map args) {
    //    // this method is called after model and view are injected
    // }

    // void mvcGroupDestroy() {
    //    // this method is called when the group is destroyed
    // }

    /*
        Remember that actions will be called outside of the UI thread
        by default. You can change this setting of course.
        Please read chapter 9 of the Griffon Guide to know more.

    def action = { evt = null ->
    }
    */
	def showAbout = { evt = null ->
		JOptionPane.showMessageDialog(app.windowManager.windows[0],
			'''Quick QR code generator''')
	}

	def paintQR = { evt = null ->
		model.enabled = false
		view.canvas.removeAll()
		doOutside {
			try {
				// model.monitorText = model.qrText
				def buttonModel = view.btns.getSelection();
				if (buttonModel!=null){
					model.level = buttonModel.getActionCommand();
				}

				String qrCodeStr = model.qrText
				def qrCode = qrCodeGenerateService.generateQRcode(qrCodeStr, model.level)
				def qrImage = qrCodeGenerateService.convertToImage(qrCode)
				def iconImage = new ImageIcon(qrImage)
				view.canvas.add(
					label(
					 icon: iconImage,
					 preferredSize: [qrImage.width, qrImage.height])
					)
				view.appFrame.pack()
			} finally {
				edt {
					model.enabled = true
				}
			}
		}
	}
}
