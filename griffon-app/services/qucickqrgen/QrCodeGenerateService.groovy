package qucickqrgen

import java.awt.Color
import java.awt.Graphics2D
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import com.google.zxing.EncodeHintType
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.common.DecoderResult
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.Decoder
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.google.zxing.qrcode.encoder.Encoder
import com.google.zxing.qrcode.encoder.QRCode

class QrCodeGenerateService {
    def serviceMethod() {

    }

	def generateQRcode(String str,  level='L'){
		QRCode qrcode = new QRCode();
		Encoder.encode(str, ErrorCorrectionLevel.valueOf(level), qrcode);
		return qrcode
	}

	def generateBitMatrix(String data, level='L'){
		int imgsize = 200;
		com.google.zxing.Writer writer = new QRCodeWriter();

		Map<EncodeHintType,?> hints = [
			(EncodeHintType.ERROR_CORRECTION):ErrorCorrectionLevel.valueOf(level)
		];

		BitMatrix matrix = writer.encode(data,
			com.google.zxing.BarcodeFormat.QR_CODE, imgsize, imgsize, hints)

		return matrix
	}

	def writeMatrix(BitMatrix matrix){
		MatrixToImageWriter.writeToFile(matrix, 'PNG', new File('testimg2.png'));
	}

	def convertToImage(QRCode qrcode, int magnify=10){
		byte[][] matrix = qrcode.matrix.array;
		int size = (qrcode.matrixWidth+2) * magnify;

		//create buffered image to draw to
		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
		image.createGraphics();
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, size, size)

		BitMatrix bm = new BitMatrix(qrcode.getMatrixWidth());

		for(int h = 0;h<qrcode.matrixWidth;h++){
			for(int w = 0;w<qrcode.matrixWidth;w++){
				//Find the colour of the dot
				if( matrix[h][w] == 0) {
					g.setColor(Color.WHITE);
				} else{
					bm.set(w, h);//build the BitMatrix
					g.setColor(Color.BLACK);
				}
				//Paint the dot
				g.fillRect((w+1)*magnify, (h+1)*magnify, magnify, magnify);
			}
		}

		return image
	}
}